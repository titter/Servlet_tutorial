/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web.jsp;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author RENT
 */
public class LinkServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private int licznik = 0;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            /*if (!Collections.list(session.getAttributeNames()).contains("licznik"))
                session.setAttribute("licznik", 1);*/
            
            if (session.isNew()){
                session.setAttribute("licznik", 1);
            }else{
                int innerLicznik = ((Integer)session.getAttribute("licznik")).intValue();
                session.setAttribute("licznik", ++innerLicznik);
                licznik++;
            }
            
            List<String> params = Collections.list(request.getParameterNames());
            /*Enumeration<String> paramNames = request.getParameterNames();
            while (paramNames.hasMoreElements()){
                
                params.add(paramNames.nextElement());
            }*/
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LinkServlet "+this.licznik+": "+session.getAttribute("licznik")+"</title>");            
            out.println("</head>");
            out.println("<body>");
            
            //out.println("<h1>Servlet LinkServlet at " + request.getContextPath() + "</h1>");
            for(String param: params){
                //out.println("Recevied: "+param+": "+request.getParameter(param));
                out.println("<h1><a href='"+request.getContextPath()+"/test?test="+licznik+"'>Hello...."+request.getParameter(param)+"</a></h1>");
            }
            out.println("</body>");
            out.println("</html>");
 
           
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletContext context = this.getServletContext();
        String dbURL = context.getInitParameter("dbURL");
        String user = context.getInitParameter("dbUser");
        String pass = context.getInitParameter("dbPassword");

        try {
            DBConnectionManager connectionManager = new DBConnectionManager(dbURL, user, pass);
            Connection con = connectionManager.getConnection();
            java.sql.PreparedStatement ps = null;
            boolean is_book= false;
            try {
                is_book = request.getParameter("is_book")!=null && request.getParameter("is_book").equals("on");
                if (is_book){
                    ps = con.prepareStatement("insert into books(title, release_date, isbn) values (?,?,?)");
                    ps.setString(1, request.getParameter("user_name"));
                    ps.setString(2, Integer.toString(LocalDateTime.now().getYear()));
                    String isbn = "ISBN"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("uuMMddN"));
                    ps.setString(3, isbn);
                }else{
                    ps = con.prepareStatement("insert into users(first_name, creation_date) values (?,?)");
                    ps.setString(1, request.getParameter("user_name"));
                    ps.setString(2, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                }
                ps.execute();
            } catch (SQLException e) {
                e.printStackTrace();
                is_book = false;
            }
            
            if (is_book){
                context.log("NEW BOOK ADDED! ["+request.getParameter("user_name")+"]");
            }else{
                context.log("NEW USER ADDED! ["+request.getParameter("user_name")+"]");
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DbServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DbServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
